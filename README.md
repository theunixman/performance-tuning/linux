# Linux Performance Tuning
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Linux Performance Tuning](#linux-performance-tuning)
    - [Network Parameters](#network-parameters)
    - [Database Parameters](#database-parameters)

<!-- markdown-toc end -->

* Kernel 4.10+

## Network Parameters
## Database Parameters
